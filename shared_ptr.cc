#include <iostream>
#include <memory>
using namespace std;

struct test
{
	int var = 100;
	test(){cout<<"Constructor called\n";}
	~test(){cout<<"Destructor called\n";}
};

shared_ptr<test> func()
{
	shared_ptr<test> sptr = make_shared<test>();
	cout<<sptr.use_count();
	//No sptr type args hence use count is 1
	//sptr is declared within the fcn
	//its assigned later
	return sptr;
}

std::shared_ptr<test> fun(std::shared_ptr<test> sh) // here since we are passing by val 
//sh takes ownership of object of test and increases the ref count by one 
//after scope of fun is reached sh gives up ownership and ref count is decremented
{
	shared_ptr<test> p2 = sh; //here it creases by one more
	//if the current ref count is one 
	//creating a shared ptr this way will increase it
	//temporarily to three and if will go back to two
	//after the scope of the function is crossed
	
	cout<<p2.use_count()<<endl;
	return p2;
}
void fun1(shared_ptr<test>& sh)
{
	//even if this fun does not allocate mem
	//ref count is increased temporarily if we pass sptr
	// as an arg by value
	cout<<sh.use_count();
}

int main()
{
	//Example 1
	shared_ptr<test> nptr = func();
	cout<<nptr.use_count();//use count is one
	//output is 1 1 Destructro called;
	///.......//
	//Example 2 by using the previous ex
	auto p2 = nptr;
	cout<<p2.use_count()<<endl; //2 
	cout<<nptr.use_count()<<endl;//2
	nptr.reset();
	cout<<p2.use_count()<<endl; // 1
	cout<<nptr.use_count()<<endl; // 0
	cout<<p2->var<<endl;
	//cout<<nptr->var; //Seg fault
	
	//Creating a shared pointer by passing a sptr by value
	//to a fun
	auto p3 = fun(p2); //3
	cout<<p2.use_count()<<endl;  // 2
	cout<<p3.use_count()<<endl;  // 2
}
