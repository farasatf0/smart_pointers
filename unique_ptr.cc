#include <iostream>
#include <memory>
using namespace std;
//unique pointer is a light weight smart pointer
class dog {
	public:
		string dog_name;
		void bark(){cout<<"Dog "<<dog_name<<" barks\n";}
		dog(string m_name){
			dog_name = m_name;
			cout<<"Dog "<<dog_name<<" is created\n";}
		~dog(){cout<<"Dog "<<dog_name<<" is killed\n";}
};

void fun(std::unique_ptr<dog> uptr)
//void fun(std::unique_ptr<dog>& uptr) if this is used
{
	uptr.reset(new dog("Monkey"));
}
void test()
{
	auto ptr = make_unique<dog>("killer");
	fun(std::move(ptr));
	ptr->bark();
	//fun(std::move(ptr)); //this is a move ops used when uptr is passed by value
	//fun(ptr);//use this, this is a copy ops used when uptr is passed by reference
	//ptr2->bark();
	//std::unique_ptr<dog> pd(new dog("Gunner"));//make_unique<dog>("Gunner");
	//pd.reset();
	//pd = make_unique<dog>("Smokey");
	//unique_ptr<dog> pd2(new dog("Smokey"));
	//pd->bark();
	//pd.release();
	//pd.reset(new dog("Monkey"));
	//pd->bark();
	//unique_ptr<dog> pd2 = move(pd);
	//pd2->bark();
	//if(pd == NULL){cout<<"pd is empty\n";}
}

int main()
{
	test();
}
/* 
 * Using reset without an object will free the memory
 * Using release will not free memory or it will not 
 * delete the object pointed by the smart pointer
 * unique pointer is passed by reference only cause
 * its copy ctor is deleted.
 */

