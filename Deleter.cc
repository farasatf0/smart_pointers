#include <iostream>
#include <memory>
using namespace std;
class test
{
    public:
         test() = default;
        ~test() {cout<<"destroyed\n";}
};
void deleter(test* ptr)
{
    delete ptr;
}
//deleter srutc is needed if you want a cusstom deleter for a unique_ptr
struct deleter_struct
{
    void operator()(test* t)
    {
        delete t;
    }
};

int main()
{
    shared_ptr<test> p(new test(), &deleter); 
    unique_ptr<test, deleter_struct>pu(new test()); //taking deleter as func ptr gives error
    return 0;
}
